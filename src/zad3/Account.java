package zad3;

public class Account {
	public String number;
	public double amount;
	public History history;

	public String getNumber() {
		return number;
	}

	public double getAmount() {
		return amount;
	}
	
	public History getHistory(){
		return history;
	}
	
	public Account(String number) {
		this.number = number;
		this.amount = 0;
		this.history = new History();
	}
	
	public void addAmount(double amount) {
		this.amount = this.amount + amount;
		this.history.add(new HistoryLog("Amount added: " + amount, OperationType.income));
	}

	public void subtract(double amount) {
		this.amount = this.amount - amount;
		this.history.add(new HistoryLog("Amount sent: " + amount, OperationType.outcome));
	}
	
	public void doOperation(Operation o) {
		o.execute(this);
	}
	

	
	
}
