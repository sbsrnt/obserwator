package zad3;

public class Bank {

	private Operation income;
	private Operation transfer;
		
	public void income(Account inAccount, double inAmount) {	
		income = new Income(inAccount, inAmount);
		inAccount.doOperation(income);
	}
	
	public void transfer(Account inFrom, Account inTo, double inAmount) { 
		transfer = new Transfer(inTo, inTo, inAmount);
		inFrom.doOperation(transfer);	
	}
}
