package zad3;

import java.util.ArrayList;
import java.util.List;

public class History {

	private List<HistoryLog> log;

	public History() {
		this.log = new ArrayList<HistoryLog>();
	}

	public void add(HistoryLog log) {
		this.log.add(log);
	}

	public List<HistoryLog> getLogs() {
		return log;
	}
	
	public String getHistory(HistoryLog h){
		return h.toString();
	}

	public void log(){
		
		for (HistoryLog h : log){
			System.out.println(h.toString());
		}
	}
}
