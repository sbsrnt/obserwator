package zad3;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HistoryLog {
	public Date dateOfOperation;
	public String title;
	public OperationType operationType;

	SimpleDateFormat getDate = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	public HistoryLog(String title, OperationType operationType) {
		this.dateOfOperation = new Date();
		this.title = title;
		this.operationType = operationType;
	}
	
	public String toString() {
        String str = "";
        
        str += "Title: " + this.title + "\n Operation type: " + this.operationType + "\n Date: " + this.getDate.format(dateOfOperation);
        
        return str;
    }
}
