package zad3;

public class Income extends Operation{

	public Account account;
	public double amount;

	public Income(Account account, double amount) {
		this.account = account;
		this.amount = amount;
	}

	@Override
	public void execute(Account account) {
		this.account.addAmount(amount);
	}

}
