package zad3;

public class Main {

	public static void main(String[] args) {
		Bank bank = new Bank();
		
		Account account1 = new Account("1");
		Account account2 = new Account("2");
		Account account3 = new Account("3");
		
		bank.income(account1, 1000);
		bank.income(account2, 200);
		bank.income(account3, 300);
		
		System.out.println("Account1 amount: " + account1.getAmount() + ", bank number: " + account1.getNumber());
		System.out.println("Account2 amount: " + account2.getAmount() + ", bank number: " + account2.getNumber());
		System.out.println("Account3 amount: " + account3.getAmount() + ", bank number: " + account3.getNumber());
		
		System.out.println("\n Account1 => Account2 \n");
		
		bank.transfer(account1, account2, 600);
		
		System.out.println("Account1 amount after transfer: " + account1.getAmount());
		System.out.println("Account2 amount after transfer: " + account2.getAmount());
		System.out.println("Account3 amount after transfer: " + account3.getAmount());
		
		System.out.println(account1.getHistory());
	}

}
