package zad3;

public abstract class Operation {
	public abstract void execute(Account account);
}
