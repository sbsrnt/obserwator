package zad3;

public class Transfer extends Operation{

	public Account from, to;
	public double amount;

	public Transfer(Account from, Account to, double amount) {
		this.from = from;
		this.to = to;
		this.amount = amount;
	}
	
	@Override
	public void execute(Account account){
		this.from = account;
		this.from.subtract(amount);
		this.to.addAmount(amount);
	}
}
